# Receive syslogs

This is a simple tool to receive syslog messages if you need this for troubleshooting.
The syslogs are parsed and printed out on stdout so you can either send them to a file
or grep them to search for what you are looking for.

This code is only a minor modification on the original example found in the
go package [gopkg.in/mcuadros/go-syslog.v2](https://github.com/mcuadros/go-syslog) to make the output readable.

## Example usage

To log all receiving messages containing *LINK-5-CHANGED* you can do something like this:
```
./rcvsyslog | grep LINK-5-CHANGED > logfile.txt
```

## Command line arguments

Some parameters can be changed. To change listen port (from default 0.0.0.0:514) specify ```-listen 127.0.0.1:514``` or the port/IP you want to listen on.

Many fields are printed out. You can review the go package documentation on what they are, but if you want to only print a subset of the messages you can do that with the parameters ```--filter-enable --filter=timestamp,content,severity```.

## How to compile

To compile for your platform download the code and its dependencies.
```
go get gopkg.in/mcuadros/go-syslog.v2
git clone https://bitbucket.org/HelgeOlav/rcvsyslog.git
cd rcvsyslog
go build
```

If you for any reason want to crosscompile for another platform you can do it like this to create a Windows executable:
```
GOOS=windows GOARCH=386 go build
```