package main

import (
	"flag"
	"fmt"
	"gopkg.in/mcuadros/go-syslog.v2"
	"os"
	"strings"
)

// global variables
var listenPort string
var filterContent bool
var filterTypes string

func init() {
	flag.StringVar(&listenPort, "listen", "0.0.0.0:514", "What to listen on")
	flag.BoolVar(&filterContent, "filter-enable", false, "enable if you want to filter out datatypes in output")
	flag.StringVar(&filterTypes, "filter", "timestamp,content", "types you want from library (key values in channel)")
	flag.Parse()
}

func main() {
	channel := make(syslog.LogPartsChannel)
	handler := syslog.NewChannelHandler(channel)

	server := syslog.NewServer()
	server.SetFormat(syslog.Automatic)
	server.SetHandler(handler)
	err := server.ListenUDP(listenPort)
	if err != nil {
		panic(err)
	}
	err = server.Boot()
	if err != nil {
		panic(err)
	}

	go func(channel syslog.LogPartsChannel) {
		acceptedContentTypes := strings.Split(filterTypes, ",")
		for logParts := range channel {
			for k, v := range logParts {
				vString := fmt.Sprintf("%v", v)
				// check if we should print this k,v
				shouldPrint := len(vString)
				// check if k is in filter list
				checkFilterResult := true
				if filterContent {
					checkFilterResult = false
					for _, fc := range acceptedContentTypes {
						if fc == k {
							checkFilterResult = true
							continue
						} // if
					} // for
				} // if
				// decide if we should print it
				if (filterContent && checkFilterResult) || (!filterContent && shouldPrint > 0) {
					fmt.Printf("%s: \"%v\" ", k, vString)
				}
			}
			fmt.Println()
		}
	}(channel)
	// start the server
	fmt.Fprintf(os.Stderr, "Logging started on %s\n", listenPort)
	server.Wait()
}
